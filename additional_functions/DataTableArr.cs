﻿using System.Data;

namespace additional_classes
{
    public class DataTableArr : DataTable
    {
        public object this[int index_row, int index_column]
        {
            get
            {
                return Rows[index_row][index_column];
            }
        }
    }
}