﻿using FirebirdSql.Data.FirebirdClient;
using System;
using System.Data;
using System.Windows.Forms;
using System.IO;
using System.Text.RegularExpressions;
using static additional_functions.additional_static_functions;

namespace library_database_application
{
    public partial class Form1 : Form //for some actions
    {
        /*this delegate can help to update 
         * datagridviews from any place of my program*/
        public delegate void update_datagridviews();
        public static update_datagridviews updation;
        
        //variable declaration
        public static FbConnection fb_con;
        public static string connection_datas =
            "Server = LocalHost; " +
            "User=SYSDBA; " +
            "Password = masterkey; ";
            //@"Database = D:\duty-rok\Documents\bd\library_database\LIBRARY.FDB";
            //@"Database = LIBRARY.FDB";
        
        //objects for data table college groups
        public static DataTable dt_college_groups;
        public static string load_all_from_dt_cg = "select * from college_groups;";
        string[] dg_cg_column_headers = {"Название группы", "Номер курса", "Количество человек"};

        //objects for data table library users
        public static DataTable dt_library_users;
        public static string load_all_from_dt_lu = "select * from library_users";
        public static string[] dg_lu_column_headers = {"№ п\\п","Фамилия","Имя","Отчество",
            "Наименование Группы","Количество взятых книг" };

        //objects for data table authors
        public static DataTable dt_authors;
        public static string load_all_from_dt_ath = "select * from authors";
        public static string[] dg_ath_column_headers = { "№ п\\п", "Фамилия", "Имя", "Отчество" };

        //objects for data table compositions
        public static DataTable dt_compositions;
        public static string load_all_from_dt_cmpth = "select * from compositions";
        string[] dg_cmpth_column_headers = {"№ п\\п","Название Произведения","Дата издания"};

        //objects for data table publisihing_houses
        public static DataTable dt_publisihing_houses;
        public static string load_all_from_dt_pbhs = "select * from publishing_houses";
        public static string[] dg_pbhs_column_headers = { "№ п\\п", "Название издательства", "Дата основания" };

        //objects for data table books
        public static DataTable dt_books;
        public static string load_all_from_dt_books = "select b.*, p.title from books b, publishing_houses p " +
            "where (p.id_publishing_house = b.id_publishing_house)";
        string[] dg_books_column_headers = {"№ п\\п","№ изд","Дата публикации","Название Книги",
            "Количество страниц","Библиотечный номер","Название издательства"};

        //methods for work with data base
        public Form1()
        {
            InitializeComponent();
        }

        // actions will be done before launch data base application
        private void Form1_Load(object sender, EventArgs e)
        {
            string path =AppDomain.CurrentDomain.BaseDirectory;
            connection_datas += "Database = " + path + "LIBRARY.FDB";
            fb_con = new FbConnection(connection_datas);

            /* init-method for datatable college_groups*/
            initalise_dt_college_groups();

            /*init-method for datatable library_users*/
            select_from_dt(fb_con, ref dt_library_users, dg_library_users, load_all_from_dt_lu);
            change_column_headers(dg_library_users,dg_lu_column_headers);

            /*init-method for datatable authors*/
            select_from_dt(fb_con, ref dt_authors, load_all_from_dt_ath);
            //change_column_headers(dg_authors, dg_ath_column_headers);

            /*init-method for datatable compositions*/
            select_from_dt(fb_con, ref dt_compositions, dg_compositions, load_all_from_dt_cmpth);
            change_column_headers(dg_compositions, dg_cmpth_column_headers);

            /*init-method for datatable publishing_houses */
            select_from_dt(fb_con, ref dt_publisihing_houses, load_all_from_dt_pbhs);
            //change_column_headers(dg_publishing_house, dg_pbhs_column_headers);

            /*init-method for datatable books*/
            select_from_dt(fb_con, ref dt_books, dg_books, load_all_from_dt_books);
            change_column_headers(dg_books, dg_books_column_headers);
            dg_books.Columns[1].Visible = false;
            
            //registration my delegate for updation datagridviews
            updation = new update_datagridviews(method_for_updation_dg);
        }

        //method for updation datagrid views on main form 
        private void method_for_updation_dg()
        {
            select_from_dt(fb_con, ref dt_college_groups, dg_college_groups, load_all_from_dt_cg);
            select_from_dt(fb_con, ref dt_library_users, dg_library_users, load_all_from_dt_lu);
        }

        private void initalise_dt_college_groups()
        {
            select_from_dt(fb_con, ref dt_college_groups, dg_college_groups, load_all_from_dt_cg);
            change_column_headers(dg_college_groups, dg_cg_column_headers);
        }

        //add group in data table college groups
        private void add_in_dt_cg_Click(object sender, EventArgs e)
        {
            add_in_cGdT form_for_addition = new add_in_cGdT(dg_college_groups);
            form_for_addition.ShowDialog();
            form_for_addition.Close();
        }

        //delete from dt college_groups
        private void del_from_dt_cg_Click(object sender, EventArgs e)
        {
            try
            {
                if ((int)dg_college_groups[2, dg_college_groups.SelectedRows[0].Index].Value != 0)
                {
                    MessageBox.Show("В группе еще есть пользователи", 
                        "Невозможно удалить", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
                string deleting_group = 
                    (string)dg_college_groups[0, dg_college_groups.SelectedRows[0].Index].Value;
                insert_or_delete_in_dt(fb_con, ref dt_college_groups, dg_college_groups,
                    $"delete from college_groups where group_name = " +
                    $"\'{deleting_group}\'", load_all_from_dt_cg);
            }
            catch (System.ArgumentOutOfRangeException)
            {
                MessageBox.Show("В этом списке пусто", "Удалять некого",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        //show window for adding in data table library_users
        private void add_in_dt_lu_Click(object sender, EventArgs e)
        {
            add_in_lUdT form_for_addition = new add_in_lUdT(dg_library_users);
            form_for_addition.ShowDialog();
            form_for_addition.Close();
        }

        //show window to sortiration on groups and delete form library_users
        private void show_on_groups_Click(object sender, EventArgs e)
        {
            form_show_on_groups form_for_show = new form_show_on_groups();
            form_for_show.ShowDialog();
            form_for_show.Close();
        }

        private void information_about_programm_Click(object sender, EventArgs e)
        {

        }

        //method for adding any composition
        private void add_in_dt_cmpth_Click(object sender, EventArgs e)
        {
            add_in_dt_cmpth form_for_adding = new add_in_dt_cmpth(dg_compositions);
            form_for_adding.ShowDialog();
            form_for_adding.Close();
        }

        //method for deleting any composition
        private void delete_from_dt_cmpth_Click(object sender, EventArgs e)
        {
            int id_del_composition;

            if (cheking_before_deleting(ref dt_compositions, "В этом списке пусто",
                "Удалять нечего")) return;
            id_del_composition =
                (int)dg_compositions[0, dg_compositions.SelectedRows[0].Index].Value;

            //delete comosition with author instead of removing author at first
            insert_or_delete_in_dt(fb_con,$"delete from authors_with_compositions " +
                $"where(id_composition = {id_del_composition})");

            //checking availabality of the composition in bwc dt
            if (cheking_before_deleting(fb_con, $"select * from BOOKS_WITH_COMPOSITIONS bwc " +
                $"where (bwc.id_composition = {id_del_composition})",
                "Сначала уберите его из книги, а лишь потом удалите",
                    "Это произведение добавлено в книгу")) return;
            //end of checking

            insert_or_delete_in_dt(fb_con, ref dt_compositions, dg_compositions,
                $"delete from compositions where id_composition = \'{id_del_composition}\';",
                load_all_from_dt_cmpth);

        }

        //method for adding somebody as author
        private void add_authors_for_cmth_Click(object sender, EventArgs e)
        {
            if (dt_compositions.Rows.Count == 0)
            {
                MessageBox.Show("У вас нет произведений\nСначала добавьте произведение",
                    "Ничего не выбрано", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            authors_compositions_relation form_for_relation =
               new authors_compositions_relation(
                   (int)dg_compositions[0, dg_compositions.SelectedRows[0].Index].Value,
                   (string)dg_compositions[1, dg_compositions.SelectedRows[0].Index].Value);
            form_for_relation.ShowDialog();
            form_for_relation.Close();
        }

        private void add_in_dt_books_Click(object sender, EventArgs e)
        {
            add_in_dt_books form_for_add = new add_in_dt_books(dg_books);
            form_for_add.ShowDialog();
            form_for_add.Close();
        }

        private void delete_from_dt_books_Click(object sender, EventArgs e)
        {
            int id_del_book;
            if (cheking_before_deleting(ref dt_books, "У вас нет ни одной книги на удаление",
                "В таблице пусто")) return;

            id_del_book = (int)dg_books[0, dg_books.SelectedRows[0].Index].Value;
            if (cheking_before_deleting(fb_con, $"select aub.* from ACTIONS_UNDER_BOOKS " +
                $"aub where(aub.id_book = {id_del_book});", "Этой книгой еще пользуются", 
                "Вы не можете списать эту книгу")) return;

            insert_or_delete_in_dt(fb_con, $"delete from books_with_compositions where " +
                $"id_book = {id_del_book}");

            insert_or_delete_in_dt(fb_con, ref dt_books, dg_books, $"delete from books where id_book=" +
                $"{id_del_book}", load_all_from_dt_books);
        }

        private void add_some_autors_Click(object sender, EventArgs e)
        {
            data_table_authors form_for_authors = new data_table_authors();
            form_for_authors.ShowDialog();
            form_for_authors.Close();
        }

        private void work_with_pub_house_Click(object sender, EventArgs e)
        {
            data_table_publishing_houses form_for_work_pbhs = new data_table_publishing_houses();
            form_for_work_pbhs.ShowDialog();
            form_for_work_pbhs.Close();
        }

        private void pbhs_with_book_relation_Click(object sender, EventArgs e)
        {
            relation_book_with_compostions form_for_show = 
                new relation_book_with_compostions((int)dg_books[0,dg_books.SelectedRows[0].Index].Value,
                (string)dg_books[3,dg_books.SelectedRows[0].Index].Value);
            form_for_show.ShowDialog();
            form_for_show.Close();
        }
    }
}
