﻿
namespace library_database_application
{
    partial class add_in_dt_books
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.page_amount = new System.Windows.Forms.NumericUpDown();
            this.library_number = new System.Windows.Forms.NumericUpDown();
            this.publishing_houses = new System.Windows.Forms.ComboBox();
            this.publication_date = new System.Windows.Forms.DateTimePicker();
            this.book_title = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.page_amount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.library_number)).BeginInit();
            this.SuspendLayout();
            // 
            // page_amount
            // 
            this.page_amount.Font = new System.Drawing.Font("DejaVu Sans Condensed", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.page_amount.Location = new System.Drawing.Point(192, 176);
            this.page_amount.Maximum = new decimal(new int[] {
            30000,
            0,
            0,
            0});
            this.page_amount.Name = "page_amount";
            this.page_amount.Size = new System.Drawing.Size(185, 23);
            this.page_amount.TabIndex = 0;
            // 
            // library_number
            // 
            this.library_number.Font = new System.Drawing.Font("DejaVu Sans Condensed", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.library_number.Location = new System.Drawing.Point(192, 246);
            this.library_number.Maximum = new decimal(new int[] {
            30000,
            0,
            0,
            0});
            this.library_number.Name = "library_number";
            this.library_number.Size = new System.Drawing.Size(185, 23);
            this.library_number.TabIndex = 1;
            // 
            // publishing_houses
            // 
            this.publishing_houses.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.publishing_houses.Font = new System.Drawing.Font("DejaVu Sans Condensed", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.publishing_houses.FormattingEnabled = true;
            this.publishing_houses.Location = new System.Drawing.Point(192, 113);
            this.publishing_houses.Name = "publishing_houses";
            this.publishing_houses.Size = new System.Drawing.Size(185, 23);
            this.publishing_houses.TabIndex = 2;
            // 
            // publication_date
            // 
            this.publication_date.Font = new System.Drawing.Font("DejaVu Sans Condensed", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.publication_date.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.publication_date.Location = new System.Drawing.Point(192, 56);
            this.publication_date.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.publication_date.Name = "publication_date";
            this.publication_date.Size = new System.Drawing.Size(185, 23);
            this.publication_date.TabIndex = 3;
            this.publication_date.Value = new System.DateTime(2021, 4, 5, 0, 0, 0, 0);
            // 
            // book_title
            // 
            this.book_title.Font = new System.Drawing.Font("DejaVu Sans Condensed", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.book_title.Location = new System.Drawing.Point(192, 9);
            this.book_title.Name = "book_title";
            this.book_title.Size = new System.Drawing.Size(185, 23);
            this.book_title.TabIndex = 4;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("DejaVu Sans Condensed", 11.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(141, 20);
            this.label1.TabIndex = 5;
            this.label1.Text = "Название Книги";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("DejaVu Sans Condensed", 11.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(12, 56);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(141, 20);
            this.label2.TabIndex = 6;
            this.label2.Text = "Дата издания";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("DejaVu Sans Condensed", 11.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(12, 113);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(141, 21);
            this.label3.TabIndex = 7;
            this.label3.Text = "Издательство";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("DejaVu Sans Condensed", 11.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.Location = new System.Drawing.Point(12, 176);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(159, 20);
            this.label4.TabIndex = 8;
            this.label4.Text = "Количество страниц";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("DejaVu Sans Condensed", 11.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label5.Location = new System.Drawing.Point(4, 246);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(167, 20);
            this.label5.TabIndex = 9;
            this.label5.Text = "Библиотечный номер";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("DejaVu Sans Mono", 15.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button1.Location = new System.Drawing.Point(89, 285);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(229, 51);
            this.button1.TabIndex = 10;
            this.button1.Text = "Добавить книгу";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // add_in_dt_books
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(400, 351);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.book_title);
            this.Controls.Add(this.publication_date);
            this.Controls.Add(this.publishing_houses);
            this.Controls.Add(this.library_number);
            this.Controls.Add(this.page_amount);
            this.Name = "add_in_dt_books";
            this.Text = "Добавить книгу";
            this.Load += new System.EventHandler(this.add_in_dt_books_Load);
            ((System.ComponentModel.ISupportInitialize)(this.page_amount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.library_number)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.NumericUpDown page_amount;
        private System.Windows.Forms.NumericUpDown library_number;
        private System.Windows.Forms.ComboBox publishing_houses;
        private System.Windows.Forms.DateTimePicker publication_date;
        private System.Windows.Forms.TextBox book_title;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button button1;
    }
}