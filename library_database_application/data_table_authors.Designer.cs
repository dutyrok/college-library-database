﻿
namespace library_database_application
{
    partial class data_table_authors
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dg_authors = new System.Windows.Forms.DataGridView();
            this.menuStrip4 = new System.Windows.Forms.MenuStrip();
            this.add_in_dt_ath = new System.Windows.Forms.ToolStripMenuItem();
            this.delete_from_dt_ath = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.dg_authors)).BeginInit();
            this.menuStrip4.SuspendLayout();
            this.SuspendLayout();
            // 
            // dg_authors
            // 
            this.dg_authors.AllowUserToAddRows = false;
            this.dg_authors.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dg_authors.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dg_authors.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dg_authors.Location = new System.Drawing.Point(0, 24);
            this.dg_authors.MultiSelect = false;
            this.dg_authors.Name = "dg_authors";
            this.dg_authors.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dg_authors.Size = new System.Drawing.Size(933, 495);
            this.dg_authors.TabIndex = 3;
            // 
            // menuStrip4
            // 
            this.menuStrip4.Font = new System.Drawing.Font("DejaVu Sans Condensed", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.menuStrip4.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.add_in_dt_ath,
            this.delete_from_dt_ath});
            this.menuStrip4.Location = new System.Drawing.Point(0, 0);
            this.menuStrip4.Name = "menuStrip4";
            this.menuStrip4.Padding = new System.Windows.Forms.Padding(7, 2, 0, 2);
            this.menuStrip4.Size = new System.Drawing.Size(933, 24);
            this.menuStrip4.TabIndex = 2;
            this.menuStrip4.Text = "menuStrip4";
            // 
            // add_in_dt_ath
            // 
            this.add_in_dt_ath.Name = "add_in_dt_ath";
            this.add_in_dt_ath.Size = new System.Drawing.Size(82, 20);
            this.add_in_dt_ath.Text = "Добавить...";
            this.add_in_dt_ath.Click += new System.EventHandler(this.add_in_dt_ath_Click);
            // 
            // delete_from_dt_ath
            // 
            this.delete_from_dt_ath.Name = "delete_from_dt_ath";
            this.delete_from_dt_ath.Size = new System.Drawing.Size(66, 20);
            this.delete_from_dt_ath.Text = "Удалить";
            this.delete_from_dt_ath.Click += new System.EventHandler(this.delete_from_dt_ath_Click);
            // 
            // data_table_authors
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(933, 519);
            this.Controls.Add(this.dg_authors);
            this.Controls.Add(this.menuStrip4);
            this.Font = new System.Drawing.Font("DejaVu Sans Condensed", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Name = "data_table_authors";
            this.Text = "Авторы произведений";
            ((System.ComponentModel.ISupportInitialize)(this.dg_authors)).EndInit();
            this.menuStrip4.ResumeLayout(false);
            this.menuStrip4.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dg_authors;
        private System.Windows.Forms.MenuStrip menuStrip4;
        private System.Windows.Forms.ToolStripMenuItem add_in_dt_ath;
        private System.Windows.Forms.ToolStripMenuItem delete_from_dt_ath;
    }
}