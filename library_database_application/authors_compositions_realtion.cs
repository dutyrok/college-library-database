﻿using System;
using System.Data;
using System.Windows.Forms;
using static additional_functions.additional_static_functions;
using static library_database_application.Form1;
using System.Collections.Generic;
using System.Linq;

namespace library_database_application
{
    public partial class authors_compositions_relation : Form
    {
        int id_composition;

        DataTable dt_available_authors, dt_exists_authors;
        string request_av_ath, request_ex_ath;

        //column headers for left and right gatagridview
        string[] both_dg_column_headers = {"","Фамилия","Имя","Отчество"}; 
            
        protected authors_compositions_relation()
        {
            InitializeComponent();
            not_change_form_size(this);
        }

        public authors_compositions_relation(int id_composition, string title) : this()
        {
            this.id_composition = id_composition;
            tilte.Text = title;
            request_av_ath = $"select a.* from authors a where not exists(" +
                $"select * from AUTHORS_WITH_COMPOSITIONS awc " +
                $"where((awc.id_author = a.id_author) and (awc.id_composition = {id_composition})));";
            request_ex_ath = $"select a.* from authors a where exists(" +
                $"select * from AUTHORS_WITH_COMPOSITIONS awc " +
                $"where((awc.id_author = a.id_author) and (awc.id_composition = {id_composition})));";
        }
        //method for updating two data grid views on form
        private void method_for_updating()
        {
            //right datagridview
            select_from_dt(fb_con, ref dt_available_authors, dg_available_authors, request_av_ath);
            
            //left datagridview
            select_from_dt(fb_con, ref dt_exists_authors, dg_exists_authors, request_ex_ath);
        }

        private void delete_authors_Click(object sender, EventArgs e)
        {
            try
            {
                insert_or_delete_in_dt(fb_con, $"delete from AUTHORS_WITH_COMPOSITIONS where(" +
                    $"(id_author = {dg_exists_authors[0, dg_exists_authors.SelectedRows[0].Index].Value}) " +
                    $"and (id_composition = {id_composition}));");

                /*after deleting records in data table AUTHORS_WITH_COMPOSITIONS
                rigth and left datagridviews need in update*/
                method_for_updating();
            }
            catch (System.ArgumentOutOfRangeException)
            {
                MessageBox.Show("Чтобы удалить автора, его сначала надо добавить", 
                    "У произведения нет авторов", 
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        //for searching in data table authors
        
        private void author_surname_TextChanged(object sender, EventArgs e)
        {
            IEnumerable<DataRow> res = dt_available_authors.AsEnumerable();
            res = from t in res where t.Field<string>(1).ToUpper().StartsWith(author_surname.Text.ToUpper()) select t;            
            if(res.Count()!=0) dg_available_authors.DataSource = res.CopyToDataTable();
        }
        //end of searching

        private void authors_compositions_realtion_Load(object sender, EventArgs e)
        {
            method_for_updating();
            change_column_headers(dg_exists_authors, both_dg_column_headers);
            change_column_headers(dg_available_authors, both_dg_column_headers);
            dg_available_authors.Columns[0].Visible = false;
            dg_exists_authors.Columns[0].Visible = false;
        }

        private void link_author_with_compos_Click(object sender, EventArgs e)
        {
            try
            {
                insert_or_delete_in_dt(fb_con, $"insert into AUTHORS_WITH_COMPOSITIONS values" +
                    $"(\'{dg_available_authors[0, dg_available_authors.SelectedRows[0].Index].Value}\'," +
                    $"\'{id_composition}\')");

                /*after adding records in data table AUTHORS_WITH_COMPOSITIONS
                rigth and left datagridviews need in update*/
                method_for_updating();
                author_surname.Text = "";
            }
            catch (System.ArgumentOutOfRangeException)
            {
                MessageBox.Show("Для того чтобы добавить автора к данному произведению\n" +
                    "Добавьте нового автора", "У вас нет доступных авторов для добавления",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
    }
}
