﻿
namespace library_database_application
{
    partial class extraditions_books_to_users
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.return_book = new System.Windows.Forms.Button();
            this.dg_given_books = new System.Windows.Forms.DataGridView();
            this.label2 = new System.Windows.Forms.Label();
            this.issue_book = new System.Windows.Forms.Button();
            this.surname = new System.Windows.Forms.Label();
            this.dg_books_for_extradition = new System.Windows.Forms.DataGridView();
            this.name = new System.Windows.Forms.Label();
            this.group = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pages_amount = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.author_name = new System.Windows.Forms.TextBox();
            this.pbhs_name = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.book_title = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.search_available = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.dg_given_books)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dg_books_for_extradition)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("DejaVu Sans Condensed", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(12, 164);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(299, 23);
            this.label3.TabIndex = 23;
            this.label3.Text = "Уже выданные книги";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("DejaVu Sans Condensed", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(461, 164);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(299, 23);
            this.label1.TabIndex = 22;
            this.label1.Text = "Книги, которые можно выдать";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // return_book
            // 
            this.return_book.Font = new System.Drawing.Font("DejaVu Sans Condensed", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.return_book.Location = new System.Drawing.Point(317, 311);
            this.return_book.Name = "return_book";
            this.return_book.Size = new System.Drawing.Size(141, 66);
            this.return_book.TabIndex = 21;
            this.return_book.Text = "Вернуть книгу";
            this.return_book.UseVisualStyleBackColor = true;
            this.return_book.Click += new System.EventHandler(this.return_book_Click);
            // 
            // dg_given_books
            // 
            this.dg_given_books.AllowUserToAddRows = false;
            this.dg_given_books.AllowUserToDeleteRows = false;
            this.dg_given_books.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dg_given_books.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dg_given_books.Location = new System.Drawing.Point(15, 190);
            this.dg_given_books.MultiSelect = false;
            this.dg_given_books.Name = "dg_given_books";
            this.dg_given_books.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dg_given_books.Size = new System.Drawing.Size(296, 187);
            this.dg_given_books.TabIndex = 20;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("DejaVu Sans Condensed", 11.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(12, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(160, 45);
            this.label2.TabIndex = 19;
            this.label2.Text = "Получатель:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // issue_book
            // 
            this.issue_book.Font = new System.Drawing.Font("DejaVu Sans Condensed", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.issue_book.Location = new System.Drawing.Point(317, 190);
            this.issue_book.Name = "issue_book";
            this.issue_book.Size = new System.Drawing.Size(141, 62);
            this.issue_book.TabIndex = 18;
            this.issue_book.Text = "Выдать книгу пользователю";
            this.issue_book.UseVisualStyleBackColor = true;
            this.issue_book.Click += new System.EventHandler(this.issue_book_Click);
            // 
            // surname
            // 
            this.surname.Font = new System.Drawing.Font("DejaVu Sans", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.surname.Location = new System.Drawing.Point(178, 8);
            this.surname.Name = "surname";
            this.surname.Size = new System.Drawing.Size(138, 45);
            this.surname.TabIndex = 17;
            this.surname.Text = "surname";
            this.surname.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // dg_books_for_extradition
            // 
            this.dg_books_for_extradition.AllowUserToAddRows = false;
            this.dg_books_for_extradition.AllowUserToDeleteRows = false;
            this.dg_books_for_extradition.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dg_books_for_extradition.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dg_books_for_extradition.Location = new System.Drawing.Point(464, 190);
            this.dg_books_for_extradition.MultiSelect = false;
            this.dg_books_for_extradition.Name = "dg_books_for_extradition";
            this.dg_books_for_extradition.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dg_books_for_extradition.Size = new System.Drawing.Size(296, 187);
            this.dg_books_for_extradition.TabIndex = 16;
            // 
            // name
            // 
            this.name.Font = new System.Drawing.Font("DejaVu Sans", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.name.Location = new System.Drawing.Point(322, 9);
            this.name.Name = "name";
            this.name.Size = new System.Drawing.Size(162, 45);
            this.name.TabIndex = 24;
            this.name.Text = "name";
            this.name.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // group
            // 
            this.group.Font = new System.Drawing.Font("DejaVu Sans", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.group.Location = new System.Drawing.Point(513, 8);
            this.group.Name = "group";
            this.group.Size = new System.Drawing.Size(138, 45);
            this.group.TabIndex = 25;
            this.group.Text = "group";
            this.group.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.panel1.Controls.Add(this.book_title);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.pbhs_name);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.pages_amount);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.author_name);
            this.panel1.Enabled = false;
            this.panel1.Location = new System.Drawing.Point(145, 58);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(614, 89);
            this.panel1.TabIndex = 26;
            // 
            // pages_amount
            // 
            this.pages_amount.Location = new System.Drawing.Point(291, 54);
            this.pages_amount.Name = "pages_amount";
            this.pages_amount.Size = new System.Drawing.Size(102, 20);
            this.pages_amount.TabIndex = 5;
            this.pages_amount.TextChanged += new System.EventHandler(this.pages_amount_TextChanged);
            // 
            // label5
            // 
            this.label5.Location = new System.Drawing.Point(291, 18);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(102, 33);
            this.label5.TabIndex = 4;
            this.label5.Text = "Укажите кол-во страниц";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(411, 22);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(186, 25);
            this.label4.TabIndex = 2;
            this.label4.Text = "Введите фамилию автора";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // author_name
            // 
            this.author_name.Location = new System.Drawing.Point(414, 54);
            this.author_name.Name = "author_name";
            this.author_name.Size = new System.Drawing.Size(183, 20);
            this.author_name.TabIndex = 1;
            this.author_name.TextChanged += new System.EventHandler(this.author_name_TextChanged);
            // 
            // pbhs_name
            // 
            this.pbhs_name.Location = new System.Drawing.Point(167, 54);
            this.pbhs_name.Name = "pbhs_name";
            this.pbhs_name.Size = new System.Drawing.Size(102, 20);
            this.pbhs_name.TabIndex = 7;
            this.pbhs_name.TextChanged += new System.EventHandler(this.pbhs_name_TextChanged);
            // 
            // label6
            // 
            this.label6.Location = new System.Drawing.Point(164, 18);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(105, 33);
            this.label6.TabIndex = 6;
            this.label6.Text = "Назавние издательства";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // book_title
            // 
            this.book_title.Location = new System.Drawing.Point(11, 54);
            this.book_title.Name = "book_title";
            this.book_title.Size = new System.Drawing.Size(131, 20);
            this.book_title.TabIndex = 9;
            this.book_title.TextChanged += new System.EventHandler(this.book_title_TextChanged);
            // 
            // label7
            // 
            this.label7.Location = new System.Drawing.Point(8, 14);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(134, 33);
            this.label7.TabIndex = 8;
            this.label7.Text = "Название книги";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // search_available
            // 
            this.search_available.Location = new System.Drawing.Point(49, 76);
            this.search_available.Name = "search_available";
            this.search_available.Size = new System.Drawing.Size(90, 58);
            this.search_available.TabIndex = 27;
            this.search_available.Text = "Искать по параметрам";
            this.search_available.UseVisualStyleBackColor = true;
            this.search_available.CheckedChanged += new System.EventHandler(this.search_available_CheckedChanged);
            // 
            // extraditions_books_to_users
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(774, 386);
            this.Controls.Add(this.search_available);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.group);
            this.Controls.Add(this.name);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.return_book);
            this.Controls.Add(this.dg_given_books);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.issue_book);
            this.Controls.Add(this.surname);
            this.Controls.Add(this.dg_books_for_extradition);
            this.Name = "extraditions_books_to_users";
            this.Text = "Выдача книг определенному пользователю";
            this.Load += new System.EventHandler(this.extraditions_books_to_users_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dg_given_books)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dg_books_for_extradition)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button return_book;
        private System.Windows.Forms.DataGridView dg_given_books;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button issue_book;
        private System.Windows.Forms.Label surname;
        private System.Windows.Forms.DataGridView dg_books_for_extradition;
        private System.Windows.Forms.Label name;
        private System.Windows.Forms.Label group;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox author_name;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox pages_amount;
        private System.Windows.Forms.TextBox book_title;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox pbhs_name;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.CheckBox search_available;
    }
}