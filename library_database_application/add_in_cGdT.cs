﻿using System;
using System.Windows.Forms;
using static additional_functions.additional_static_functions;
using static library_database_application.Form1;

namespace library_database_application
{
    public partial class add_in_cGdT : Form
    {
        DataGridView dg_college_groups;

        public add_in_cGdT()
        {
            InitializeComponent();
            //FormBorderStyle = FormBorderStyle.FixedSingle;
            //MaximizeBox = false;
            not_change_form_size(this);
        }
        public add_in_cGdT(DataGridView dataGridView) : this()
        {
            this.dg_college_groups = dataGridView;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (textBox1.Text.Length == 0)
            {
                MessageBox.Show("Вы не дали название для группы", "Не указаны нужные данные",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            if (textBox1.Text.Length > 20)
            {
                MessageBox.Show("Нужно указать группу длиной менее 20", "Cлишком длинное название",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            if (checking_on_other_table<string>(textBox1.Text, dt_college_groups, 0))
            {
                MessageBox.Show($"У вас уже есть группа {textBox1.Text}", "Такая группа уже есть",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            insert_or_delete_in_dt(fb_con, ref dt_college_groups, dg_college_groups,
                "insert into college_groups values" +
                $"(\'{textBox1.Text}\',\'{numericUpDown1.Value}\',\'0\');", load_all_from_dt_cg);
            textBox1.Text = "";
        }
    }
}
