﻿using System;
using System.Windows.Forms;
using static additional_functions.additional_static_functions;
using static library_database_application.Form1;

namespace library_database_application
{
    public partial class add_in_dt_ath : Form
    {
        DataGridView dataGridView;
        public add_in_dt_ath()
        {
            InitializeComponent();
            not_change_form_size(this);
        }

        /// <summary>
        /// Construct for adding in authors' data table form
        /// </summary>
        /// <param name="dataGridView">datagridview for 
        /// display information from data table authors</param>
        public add_in_dt_ath(DataGridView dataGridView) : this()
        {
            this.dataGridView = dataGridView;
        }

        //method for adding
        private void button1_Click(object sender, EventArgs e)
        {
            if(surname.Text=="" || author_name.Text == "")
            {
                MessageBox.Show("Возможно вы не указали фамилию или имя", "Вы что-то не указали",
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            insert_or_delete_in_dt(fb_con, ref dt_authors, dataGridView, $"insert into authors " +
                $"values(\'{autoincrement_id(ref dt_authors,0)}\'," +
                $"\'{surname.Text}\',\'{author_name.Text}\'," +
                $"\'{patronymic.Text}\');",load_all_from_dt_ath);
            surname.Text = "";
            author_name.Text = "";
            patronymic.Text = "";
        }
    }
}
