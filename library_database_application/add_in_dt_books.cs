﻿using System;
using System.Windows.Forms;
using static additional_functions.additional_static_functions;
using static library_database_application.Form1;

namespace library_database_application
{
    public partial class add_in_dt_books : Form
    {
        int[] array_of_pb_index;
        DataGridView dataGridView;
        
        protected add_in_dt_books()
        {
            InitializeComponent();
            not_change_form_size(this);
        }

        public add_in_dt_books(DataGridView dataGridView) : this()
        {
            this.dataGridView = dataGridView;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (book_title.Text.Length == 0 || publishing_houses.Text.Length == 0 || 
                page_amount.Value == 0)
            {
                MessageBox.Show("Перепроверьте введенную информацию\n" +
                    "Возможно вы что-то упустили или ввели некоректно", "Вы ошиблись при вводе", 
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            if (checking_on_other_table((int)library_number.Value, dt_books, 5))
            {
                MessageBox.Show("Уже есть книга с таким номером",
                    "Невозможно использовать данный библиотечный номер",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            insert_or_delete_in_dt(fb_con, ref dt_books, dataGridView, $"insert into books values(" +
                $"\'{autoincrement_id(ref dt_books,0)}\',\'{array_of_pb_index[publishing_houses.SelectedIndex]}\'," +
                $"\'{publication_date.Value.ToShortDateString()}\',\'{book_title.Text}\',\'{page_amount.Value}\',\'{library_number.Value}\');", 
                load_all_from_dt_books);
            book_title.Text = "";
            page_amount.Value = 0;
        }

        private void add_in_dt_books_Load(object sender, EventArgs e)
        {
            publication_date.Value = DateTime.Now;
            array_of_pb_index = read_title_id_to_combobox<int>(ref publishing_houses,dt_publisihing_houses,1,0);
        }
    }
}
