﻿
namespace library_database_application
{
    partial class relation_book_with_compostions
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.delete_cmps = new System.Windows.Forms.Button();
            this.dg_exists_composition = new System.Windows.Forms.DataGridView();
            this.label2 = new System.Windows.Forms.Label();
            this.link_cmps_with_book = new System.Windows.Forms.Button();
            this.title = new System.Windows.Forms.Label();
            this.dg_available_compositions = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.dg_exists_composition)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dg_available_compositions)).BeginInit();
            this.SuspendLayout();
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("DejaVu Sans Condensed", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(12, 54);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(299, 23);
            this.label3.TabIndex = 15;
            this.label3.Text = "Произведения, которые уже добавлены";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("DejaVu Sans Condensed", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(461, 54);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(299, 23);
            this.label1.TabIndex = 14;
            this.label1.Text = "Произведения, которые можно добавить";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // delete_cmps
            // 
            this.delete_cmps.Font = new System.Drawing.Font("DejaVu Sans Condensed", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.delete_cmps.Location = new System.Drawing.Point(317, 210);
            this.delete_cmps.Name = "delete_cmps";
            this.delete_cmps.Size = new System.Drawing.Size(141, 66);
            this.delete_cmps.TabIndex = 13;
            this.delete_cmps.Text = "Удалить произведение из книги";
            this.delete_cmps.UseVisualStyleBackColor = true;
            this.delete_cmps.Click += new System.EventHandler(this.delete_cmps_Click);
            // 
            // dg_exists_composition
            // 
            this.dg_exists_composition.AllowUserToAddRows = false;
            this.dg_exists_composition.AllowUserToDeleteRows = false;
            this.dg_exists_composition.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dg_exists_composition.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dg_exists_composition.Location = new System.Drawing.Point(15, 89);
            this.dg_exists_composition.MultiSelect = false;
            this.dg_exists_composition.Name = "dg_exists_composition";
            this.dg_exists_composition.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dg_exists_composition.Size = new System.Drawing.Size(296, 187);
            this.dg_exists_composition.TabIndex = 12;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("DejaVu Sans Condensed", 11.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(12, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(276, 45);
            this.label2.TabIndex = 11;
            this.label2.Text = "Книга:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // link_cmps_with_book
            // 
            this.link_cmps_with_book.Font = new System.Drawing.Font("DejaVu Sans Condensed", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.link_cmps_with_book.Location = new System.Drawing.Point(317, 89);
            this.link_cmps_with_book.Name = "link_cmps_with_book";
            this.link_cmps_with_book.Size = new System.Drawing.Size(141, 62);
            this.link_cmps_with_book.TabIndex = 10;
            this.link_cmps_with_book.Text = "Добавить произведение в книгу";
            this.link_cmps_with_book.UseVisualStyleBackColor = true;
            this.link_cmps_with_book.Click += new System.EventHandler(this.link_cmps_with_book_Click);
            // 
            // title
            // 
            this.title.Font = new System.Drawing.Font("DejaVu Sans", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.title.Location = new System.Drawing.Point(317, 9);
            this.title.Name = "title";
            this.title.Size = new System.Drawing.Size(443, 45);
            this.title.TabIndex = 9;
            this.title.Text = "title";
            this.title.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // dg_available_compositions
            // 
            this.dg_available_compositions.AllowUserToAddRows = false;
            this.dg_available_compositions.AllowUserToDeleteRows = false;
            this.dg_available_compositions.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dg_available_compositions.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dg_available_compositions.Location = new System.Drawing.Point(464, 89);
            this.dg_available_compositions.MultiSelect = false;
            this.dg_available_compositions.Name = "dg_available_compositions";
            this.dg_available_compositions.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dg_available_compositions.Size = new System.Drawing.Size(296, 187);
            this.dg_available_compositions.TabIndex = 8;
            // 
            // relation_book_with_compostions
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(775, 292);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.delete_cmps);
            this.Controls.Add(this.dg_exists_composition);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.link_cmps_with_book);
            this.Controls.Add(this.title);
            this.Controls.Add(this.dg_available_compositions);
            this.Name = "relation_book_with_compostions";
            this.Text = "Добавление произведений в Книги";
            this.Load += new System.EventHandler(this.relation_book_with_compostions_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dg_exists_composition)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dg_available_compositions)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button delete_cmps;
        private System.Windows.Forms.DataGridView dg_exists_composition;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button link_cmps_with_book;
        private System.Windows.Forms.Label title;
        private System.Windows.Forms.DataGridView dg_available_compositions;
    }
}